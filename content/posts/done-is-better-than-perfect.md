---
title: Done is better than perfect
description: Getting this show on the road
img: first-blog-post.jpg
alt: my first blog post
author:
  name: Brad Zeffren
  date: 3/23/21
---

## Done is better than perfect

So I can be a bit of a perfectionist.. or at least an over-analyst. It's taken me the better part of four decades to realize that this personality trait is not always constructive. When starting any kind of creative project, the tendency for me has been to charge in with a full head of steam and inspiration.

In the beginning, things generally go pretty great. Then, about halfway through, the super-zoom lens of the perfectionist kicks in, and that's when things start bogging down in details I never knew I cared about. Often times, I end up with another partially finished idea. But whether it's a web project, an article, a music track, etc. doesn't matter. If it's not finished, then it's just not finished. It really doesn't matter what media it is, or how good it is.

As a project, this website you're on now has been on my list of things to do for a long, long time. I wanted a vehicle to try out some dev and design ideas, post an online CV, publish some articles about things I've learned over the years, try some experiments with front-end web tech, and more. When I got started, I scrawled this phrase -- "Done is better than perfect" -– onto a piece of tape and stuck it on my monitor. Honestly, it worked.

I've tried other little motivational phrases, but this one actually did the trick. Whenever I would find myself peering over the precipice of a brand new rabbit hole, I'd look up and see it.. "Done is better than perfect". And _most_ of the time I successfully forced myself to pass over the rabbit hole and keep going.

So I will get around to writing those next few pieces I have planned out (posts about front-end techniques, or observations on professional tech culture, or even [how I put this site together...](https://gitlab.com/bzeffren/saturated-site)), I will also do some experimental graphics work and document it, and probably some other things that may or may not be interesting. But that's later. For now: "Done is better than perfect"
