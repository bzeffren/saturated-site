export const resumeEntries = [
  {
    id: 'rms',
    org: 'RMS',
    location: 'SF Bay Area, CA',
    title: 'Staff Engineer',
    years: '2018 - Present',
    description: `Risk Management Solutions is the world's premier provider of catastrophic risk modeling. The company is almost 30 years old, and began by only providing statistical models. These last few years have seen the company shift to SaaS based services. As a Staff Engineer focused on the front-end, most of my time is spent getting new features into customers hands as a member and technical lead of a product team. I've also been a core contributor to the cross-team design system and UI component library. `,
  },
  {
    id: 'emma',
    org: 'Emma/CM Group',
    location: 'Nashville, TN',
    title: 'Sr. Engineer / UX',
    years: '2014 - 2018',
    description: `Emma was one of the first SaaS companies in Nashville's young tech community as an early provider of email marketing tools. My roles there were varied over my five year tenure, starting out as a UX designer and ultimately ending up as a Senior Engineer focusing on front-end development. It was one of those special, 'right place at the right time' companies, and I learned a ton while working there. It was eventually acquired by Campaign Monitor and continues to thrive as part of CM Group.`,
  },
  {
    id: 'civilization',
    org: 'Civilization',
    location: 'Seattle, WA',
    title: 'Engineer / UX',
    years: '2012 - 2014',
    description: `Civilization is a boutique creative agency focusing on graphic design and interactive development. My work there was varied, though it mostly centered on UX prototyping and CMS backed web development (Wordpress, Shopify, Drupal, etc..). As a multi-disciplinary agency, I was also involved in video editing, set design, and site installation.`,
  },
  {
    id: 'music',
    org: 'Music Production',
    location: 'Boston/Seattle/Nashville',
    title: 'Producer / Engineer / Artist',
    years: '1994 - Present',
    description: `I started working in recording studios when I was 19, and spent the next 20-ish years in and out of them. After graduating from Berklee College of Music in 1999, I left Boston and moved west to Seattle where I worked in, owned, and freelanced in various studios as a Producer/Engineer. Of course, I had a band, and played in bands, and lived the music life. It's still something I work on every time I get the chance.`,
  },
]
