import { gsap } from 'gsap'
import { random } from 'lodash'

export const toggleOpen = (el, prop, toggleParam, breakpoint = 1024) => {
  const duration = 0.3
  const openEase = 'sine.out(2.7)'
  const closeEase = 'sine.in(1.7)'
  const animatedProp = window.innerWidth > breakpoint ? prop : 'height'

  const setAuto = () => gsap.set(`${el}`, { [animatedProp]: 'auto' })

  if (toggleParam) {
    // Opening
    setAuto()
    gsap.from(`${el}`, {
      [animatedProp]: 0,
      duration,
      ease: openEase,
      onComplete: setAuto,
    })
    gsap.to(`${el}`, {
      opacity: 1,
      duration,
      delay: 0.4,
    })
  } else {
    // Closing
    gsap.to(`${el}`, { opacity: 0, duration })
    gsap.to(`${el}`, {
      [animatedProp]: 0,
      duration,
      ease: closeEase,
      delay: 0.1,
    })
  }
}

export const pulseOpacity = (el) => {
  const els = document.querySelectorAll(el)

  els.forEach((el) =>
    gsap.to(el, {
      opacity: 0.8,
      duration: random(6, 12),
      repeat: -1,
      delay: random(0, 4),
      repeatDelay: random(0, 4),
      yoyo: true,
    })
  )
}
