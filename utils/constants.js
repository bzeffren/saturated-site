export const SECTIONS = {
  ABOUT: 'about',
  BLOG: 'blog',
  CONTACT: 'contact',
  RESUME: 'resume',
}

export const EVENTS = {
  ACTIVE_CHANGE: 'active-change',
}
